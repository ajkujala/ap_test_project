﻿using System;
using System.Collections.Generic;	// List == std::vector


namespace napaExample
{
	class napaExample{
		public static void Main()
		{
			
			Point p11 = new Point (0, 0, 0);
			Point p12 = new Point (4, 0, 0);	// Closest point to p2
			Point p13 = new Point (0,4,0);	

			Polygon p1 = new Polygon (new List<Point>() {p11, p12, p13});
			Polygon p1r = new Polygon (new List<Point>() {p13, p12, p11});	// reverse p1
	
			Point p21 = new Point (5,0,0); 	// Closest point to p1
			Point p22 = new Point (5,5,0);
			Point p23 = new Point (7,6,0);
			Point p24 = new Point (8,4,0);

			Polygon p2 = new Polygon (new List<Point> () { p21, p22, p23, p24 });
			Polygon p2r = new Polygon (new List<Point> () { p24, p23, p22, p21 }); // reverse p2

			Point p31 = new Point (1, -1, 0);	// closest point to p1
			Point p32 = new Point (6, -1, -2);
			Point p33 = new Point (5, -3, 2);

			Polygon p3 = new Polygon (new List<Point> () { p31, p32, p33 });

			Point p41 = new Point (4, 4, 2);
			Point p42 = new Point (4, 4, -2);
			Point p43 = new Point (3, 4, -2);

			Polygon p4 = new Polygon (new List<Point> () { p41, p42, p43 });

			Point p51 = new Point (1, 1, 0);
			Point p52 = new Point (-1 ,1, 0);
			Point p53 = new Point (1, 2, 0);

			Polygon p5 = new Polygon (new List<Point>() {p51, p52, p53});

			Polygon p6 = new Polygon (new List<Point> () { p12, p11, p51 });

			Console.WriteLine ("Test 01");
			Tuple<Point, Point, double> closest1 = Polygon.getClosestPoints (p1, p2);
			Console.WriteLine (closest1);
			Console.WriteLine (closest1.Item1 == p12);	// True
			Console.WriteLine (closest1.Item2 == p21);	// True
			Console.WriteLine (closest1.Item1 == p11);	// False
			Console.WriteLine (closest1.Item2 == p22);	// False
			Console.WriteLine ("First segment dir of p1: " + p1.getSegmentDirection(0));
			Console.WriteLine ("second segment dir of p1: " + p1.getSegmentDirection (1));
			Console.WriteLine ("First segment dir of p2: " + p2.getSegmentDirection(0));
			Console.WriteLine ("last segment dir of p2: " + p2.getSegmentDirection (3));
			Console.WriteLine (p1.getDirection (closest1.Item1));
			Console.WriteLine (p2.getDirection (closest1.Item2));

			Console.Read ();
			Console.WriteLine ("Test 02 - reversed polygons");

			Tuple<Point, Point, double>closest2 = Polygon.getClosestPoints (p1r, p2r);
			Console.WriteLine (closest2);
			Console.WriteLine (p1r.getDirection (closest2.Item1));
			Console.WriteLine (p2r.getDirection (closest2.Item2));

			Console.Read ();
			Console.WriteLine ("Test 03 - p2 reversed");

			Tuple<Point, Point, double>closest3 = Polygon.getClosestPoints (p1, p2r);
			Console.WriteLine (closest3);
			Console.WriteLine (p1.getDirection (closest3.Item1));
			Console.WriteLine (p2r.getDirection (closest3.Item2));

			Console.Read ();
			Console.WriteLine ("Test 04 - p1 reversed");

			Tuple<Point, Point, double>closest4 = Polygon.getClosestPoints (p1r, p2);
			Console.WriteLine (closest4);
			Console.WriteLine (p1r.getDirection (closest4.Item1));
			Console.WriteLine (p2.getDirection (closest4.Item2));

			Console.Read ();
			Console.WriteLine ("Test 05 - p1 to p3");

			Tuple<Point, Point, double>closest5 = Polygon.getClosestPoints (p1, p3);
			Console.WriteLine (closest5);
			Console.WriteLine (p1.getDirection (closest5.Item1));
			Console.WriteLine (p3.getDirection (closest5.Item2));

			Console.Read ();
			Console.WriteLine ("Test 06 - p2 to p3");

			Tuple<Point, Point, double>closest6 = Polygon.getClosestPoints (p2, p3);
			Console.WriteLine (closest6);
			Console.WriteLine (p2.getDirection (closest6.Item1));
			Console.WriteLine (p3.getDirection (closest6.Item2));

			Console.Read ();
			Console.WriteLine ("Test 07 - p2 to p4 (closest points on segments)");

			Tuple<Point, Point, double>closest7 = Polygon.getClosestPoints (p2, p4);
			Console.WriteLine (closest7);
			Console.WriteLine (p2.getDirection (closest7.Item1));
			Console.WriteLine (p4.getDirection (closest7.Item2));

			Console.Read ();
			Console.WriteLine ("Test 08 - p1 to p5 (Crossing polygons)");

			Tuple<Point, Point, double>closest8 = Polygon.getClosestPoints (p1, p5);
			Console.WriteLine (closest8);
			Console.WriteLine (p1.getDirection (closest8.Item1));
			Console.WriteLine (p5.getDirection (closest8.Item2));


			Console.Read ();
			Console.WriteLine ("Test 09 - p1 to p1 (Same polygon)");

			Tuple<Point, Point, double>closest9 = Polygon.getClosestPoints (p1, p1);
			Console.WriteLine (closest9);
			Console.WriteLine (p1.getDirection (closest9.Item1));
			Console.WriteLine (p1.getDirection (closest9.Item2));

			Console.Read ();
			Console.WriteLine ("Test 10 - Shared segment");

			Tuple<Point, Point, double>closest10 = Polygon.getClosestPoints (p1, p6);
			Console.WriteLine (closest10);
			Console.WriteLine (p1.getDirection (closest10.Item1));
			Console.WriteLine (p6.getDirection (closest10.Item2));

		}	
	}

	/*
	 * Point class
	 * Or a simple vector class =) :w
	 */
	class Point{

		// Members

		private double[] coords = new double[4];

		// Constructors
		// Default constructor
		public Point() {}

		// Constructor with x, y, z values as double
		public Point(double x, double y, double z){
			this.coords [0] = x;
			this.coords [1] = y;
			this.coords [2] = z;
			this.coords [3] = 0.0;	// Padding or weight for NURBS
		}	

		// Setters
		// Getters

		// Get x, y, z or w by int
		public double getNth(int i){
			return this.coords [i];
		}

		// Get distance between points
		// input:	Point p - point to measure distance
		// output:	double - distance
		public double distance(Point p){
			return Math.Sqrt (sqrDistance(p));
		}

		// Get squared distance
		// input:	Point p - another point to measure distance
		// outpit:	double - distance squared
		public double sqrDistance(Point p){
			double s = 0.0;
			for (int i = 0; i < 3; ++i) {
				s += Math.Pow((this.coords [i] - p.getNth(i)), 2);
			}	
			return s;
		}

		// Get length
		// output:	double as distance from origo (or vector length)
		public double getLength(){
			return distance (new Point ());
		}

		// Get unit vector
		// output:	Point - as unit vector
		public Point getUnit(){
			return this * (1 / this.getLength ());
		}

		// Methods


		// Dot product
		// input:	Point p - Point as vector
		// output:	double - return dot product
		public double dot(Point p){
			double d = 0.0;
			for (int i=0; i<3; ++i){
				d += coords [i] * p.getNth (i);
			}
			return d;
		}

		// Projection to line segment
		// If outside, snaps to vertex
		// input:	Point p1, p2 - Segment points
		// output:	Point - Projected to segment
		public Point project(Point p1, Point p2){

			Point line, toLine;

			if (this.sqrDistance (p1) < this.sqrDistance (p2)) {
				line = p2 - p1;
				toLine = this - p1;
			} else {
				line = p1 - p2;
				toLine = p1 - this;
			}
			double s = line.dot (line);
			double t = toLine.dot (line) / s;
			//Console.WriteLine (line);
			//Console.WriteLine (this);
			//Console.WriteLine ("s: " + s + ", t: " + t); 

			// Snap to end points
			if (t < 0.0) {return p1;}
			if (t > 1.0) {return p2;}
			return (p2 * t) + (p1 * (1-t));
		}

		// Project point to segment 
		// input:	Tuple<Point, Point> segment - the segment points
		// output:	Point - Projectes to Segment
		public Point project(Tuple<Point, Point> segment){
			return this.project (segment.Item1, segment.Item2);
		}


		// Override string
		public override string ToString(){
			return "[" + this.coords[0] + ", " + this.coords[1] + ", " + this.coords[2] + "]";
		}

		public override bool Equals(Object o){
			if (o == null || !(o is Point)) {return false;}
			return (Point)o == this;
		}

		public override int GetHashCode(){
			return (int)(this.coords [0] + this.coords [1] + this.coords [2]);
		}

		// Static methods

		// Add two points
		public static Point operator +(Point a, Point b){
			return new Point (b.getNth (0) + a.getNth (0), 
				b.getNth (1) + a.getNth (1), 
				b.getNth (2) + a.getNth (2));
		}

		// Substract points
		public static Point operator -(Point a, Point b){
			return new Point (a.getNth (0) - b.getNth (0), 
				a.getNth (1) - b.getNth (1), 
				a.getNth (2) - b.getNth (2));
		}

		// Multiply by scalar
		public static Point operator *(Point a, double s){
			return new Point (a.getNth (0) * s, 
				a.getNth (1) * s, 
				a.getNth (2) * s);
		}

		public static Point operator *(double s, Point a){
			return new Point (a.getNth (0) * s, 
				a.getNth (1) * s, 
				a.getNth (2) * s);
		}

		// Compare operators
		public static bool operator ==(Point a, Point b){
			return Math.Abs (a.distance (b)) < 1e-3;
		}	

		public static bool operator !=(Point a, Point b){
			return !(a == b); 
		}	

	}




	/*
	 * Polygon class
	 * Assumes planarity or clossed polyline
	 */
	class Polygon{
		// Members

		private List<Point> points; 			// List of points in the polygon

		// Constuctor
		public Polygon(){
			this.points = new List<Point> ();
		}	

		// 
		public Polygon(List<Point> points){
			this.points = points;
		}

		// Setters
		// Add point to list
		// input:	Point p - point to be added
		public void addPoint(Point p){
			this.points.Add (p);
		}

		// Getters
		// input:	int i- as index of the point
		public Point getNthPoint(int i){
			i = i % this.points.Count;
			return this.points[i];
		}

		// Get points of segment
		// intput:	int i - nth segment
		// output:	Tuple<Point, Point> - as start and end point
		public Tuple<Point, Point> getNthSegment(int i){
			return new Tuple<Point, Point>(getNthPoint(i), getNthPoint(i+1));
		}

		// Get number of points
		// output:	int - number of points in polygon
		public int count(){
			return this.points.Count;
		}

		// Get index of point
		// intput:	Point p - point to lookup
		// outpit: 	int - index of the point. Negatice if not found
		public int getIndex(Point p){
			return this.points.IndexOf (p);
		}

		// Returns segment leght
		// input:	int i - nth segment
		// output:	double - length of the segment
		public double getSegmentLength(int i){
			Tuple<Point, Point> t = this.getNthSegment (i);
			return t.Item1.distance (t.Item2);
		}

		// Return direction of segment
		// intput:	int i - nth segment 
		// output:	Point . as unit vector
		public Point getSegmentDirection(int i){
			Tuple <Point, Point> t = this.getNthSegment (i);
			Point p = t.Item2 - t.Item1;
			return p.getUnit ();
		}


		// Return direction of vertex. Unit vector of segment[i] +  segment[i-1]
		// intput:	int i - nth vertex
		// output:	Point - point as unit vector
		public Point getVertexDirection(int i){
			Point p1= this.getSegmentDirection (i);
			Point p2 = i == 0 ? this.getSegmentDirection(this.points.Count-1) : this.getSegmentDirection(i-1);
			return (p1 + p2).getUnit();
		}


		// Return point at location t
		// input:	double t - as parameter
		// output:	Point - the point in segment
		public Point getPointAtT(int i, double t){
			Tuple <Point, Point> segment = getNthSegment (i);
			return segment.Item2 * t + segment.Item1 * (1 - t);
		}

		// Get direction of the polygon at point p
		// input - Point p - point of polygon segment
		// output - Point as direction vector or null if point not at the segment or vertex
		public Point getDirection(Point p){
			for (int i = 0; i < this.count (); ++i) {
				if (p == this.getNthPoint (i)) {
					return this.getVertexDirection (i);
				}
			}
			for (int i = 0; i < this.count (); ++i) {
				if (this.getSegmentDirection (i) == (this.getNthSegment (i).Item2 - p).getUnit ()) {
					return this.getSegmentDirection (i);
				}
			}
			return null;
		}

		// gradient 
		// Returns delta distance between segments 
		// input: 	int i - nth segment of the polygon
		// 			double t - parameter of the point 
		//			Tuple<Point, Point> - Segment to measure
		//			double dt - half step for gradient
		// output:	double - the change of distance
		public double getGradient(int i, double t, Tuple<Point, Point> segment, double dt = 1e-12){
			Tuple <Point, Point> ownSegment = this.getNthSegment(i);
			Point p1 = ownSegment.Item2*(t-dt) + ownSegment.Item1*(1-(t-dt));
			Point p2 = ownSegment.Item2*(t+dt) + ownSegment.Item1*(1-(t+dt));
			double dl = (p2.sqrDistance (p2.project (segment)) - p1.sqrDistance (p1.project (segment)))/ dt;
			return dl;

		}

		// Search closest points between two segment 
		// Returns closest points and distance in tuple<Point, Point, double>
		// intput:	int i - nth segment of the self
		//			int j - nth segment of "foreign" polygon
		//			Polygon - "foreign" polygon
		// 			double eps - accuracy [optional]
		//			int iter - maximimum iterations [optional]
		// output:	Tuple<Point, Point, double> - closest point between segments and distance
		public Tuple<Point, Point, double> closestPointsBetweenSegments(int i, int j, Polygon poly, double eps = 1e-24, int iter=100){
			double lowT = 0.0;
			double highT = 1.0;
			double t = (highT + lowT) / 2;
			double dl = this.getGradient (i, t, poly.getNthSegment (j));
			double olddl = 0.0;

			// Binary search
			while (iter >= 0) {
				if (Math.Abs (dl-olddl) < eps) {
					break;
				} else if (dl < 0.0) {
					lowT = t;
				} else {
					highT = t;
				}
				olddl = dl;
				t = (highT + lowT) / 2;
				dl = this.getGradient (i, t, poly.getNthSegment (j));

				--iter;
			}

			Point myPoint = this.getPointAtT(i, t);
			Point closestPoint = myPoint.project (poly.getNthSegment (j));
			return new Tuple<Point, Point, double> (myPoint, closestPoint, myPoint.distance(closestPoint));
		}

		// Static methods

		// Static method to find closest point between two polygons
		// input:	Polygon p1, p2
		// output:	Tuple<Point, Point, double> as closest point between polygons and distance
		public static Tuple<Point, Point, double> getClosestPoints (Polygon p1, Polygon p2){
			Tuple<Point, Point, double> closest = p1.closestPointsBetweenSegments (0, 0, p2);

			// O(n^2) 
			for (int i = 0; i < p1.count (); ++i) {
				for (int j = 0; j < p2.count (); ++j) {
					Tuple<Point, Point, double> candidate = p1.closestPointsBetweenSegments (i, j, p2);
					// Console.WriteLine (candidate.Item3 + ", " + i + ", " + j);
					if (candidate.Item3 < closest.Item3) {
						closest = candidate;
					}
				}
			}
			return closest;
		}
	}
}